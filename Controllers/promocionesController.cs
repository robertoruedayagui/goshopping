﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_dsw1.Data;
using Proyecto_dsw1.utils;
using Proyecto_dsw1.Entity;

namespace Proyecto_dsw1.Controllers
{
    public class promocionesController : Controller
    {
        private proyectoEntities db = new proyectoEntities();

        // GET: promociones
        public ActionResult Index()
        {
            ViewBag.paginaPadre = "Promociones";
            ViewBag.pagina = "Listado";

            if (verificarSession.sessionEmpleado("Promociones") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if (verificarSession.sessionEmpleado("Promociones") == Constante.NO_AUTORIZADO)
                return RedirectToAction("LoginAdmin", "Home");
            else
            {
                var promociones = db.promociones.Include(p => p.producto);
                var lista = db.sp_promocion_vigente().ToList();
                var productos = db.producto.ToList();
                foreach (sp_promocion_vigente_Result item in lista)
                    productos.RemoveAll(x => x.id_prod == item.id_prod);

                ViewBag.in_id_producto = new SelectList(productos, "id_prod", "nom_prod");
                return View(promociones.ToList());
            }
        }

        // GET: promociones/Create
        public ActionResult Create()
        {
            if (verificarSession.sessionEmpleado("Promociones") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if (verificarSession.sessionEmpleado("Promociones") == Constante.NO_AUTORIZADO)
                return RedirectToAction("LoginAdmin", "Home");
            else
            {
                ViewBag.paginaPadre = "Promociones";
                ViewBag.pagina = "Nuevo";
                ViewBag.in_id_producto = new SelectList(db.producto, "id_prod", "nom_prod");
                return View();
            }
        }

        // POST: promociones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "vc_descripcion,in_id_producto,vc_tipo,dc_prec_desc,dt_fec_ini,dt_fec_fin,vc_user_reg,dt_fec_reg")] promociones promociones)
        {
            if (verificarSession.sessionEmpleado("Promociones") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if (verificarSession.sessionEmpleado("Promociones") == Constante.NO_AUTORIZADO)
                return RedirectToAction("LoginAdmin", "Home");
            else
            {
                ViewBag.paginaPadre = "Promociones";
                ViewBag.pagina = "Nuevo";
                login emp = (login)Session["usuario"];
                if (ModelState.IsValid)
                {
                    if (promociones.vc_tipo == Constante.SOLES.ToString())
                    {
                        promociones.dc_prec_desc = promociones.dc_prec_desc;
                        promociones.dc_porc_desc = 0;
                    }
                    else
                    {

                        promociones.dc_porc_desc = promociones.dc_prec_desc;
                        promociones.dc_prec_desc = 0;
                    }
                    promociones.vc_estado = "ACT";
                    promociones.vc_user_reg = emp.vc_usuario;
                    promociones.dt_fec_reg = DateTime.Now;

                    db.promociones.Add(promociones);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.in_id_producto = new SelectList(db.producto, "id_prod", "vc_cod_prod", promociones.in_id_producto);
                return View(promociones);
            }
        }

        // GET: promociones/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.paginaPadre = "Promociones";
            ViewBag.pagina = "Nuevo";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            promociones promociones = db.promociones.Find(id);
            if (promociones == null)
            {
                return HttpNotFound();
            }
            ViewBag.in_id_producto = new SelectList(db.producto, "id_prod", "vc_cod_prod", promociones.in_id_producto);
            return View(promociones);
        }

        // POST: promociones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "in_id_promocion,vc_descripcion,in_id_producto,vc_tipo,dc_prec_desc,dc_porc_desc,dt_fec_ini,dt_fec_fin,vc_user_reg,dt_fec_reg,vc_user_mod,dt_fec_mod")] promociones promociones)
        {
            if (ModelState.IsValid)
            {
                db.Entry(promociones).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.in_id_producto = new SelectList(db.producto, "id_prod", "vc_cod_prod", promociones.in_id_producto);
            return View(promociones);
        }

        // GET: promociones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            promociones promociones = db.promociones.Find(id);
            if (promociones == null)
            {
                return HttpNotFound();
            }
            return View(promociones);
        }

        // POST: promociones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            promociones promociones = db.promociones.Find(id);
            db.promociones.Remove(promociones);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public JsonResult obtenerProducto(int id_prod)
        {
            BEItemGenerico result = new BEItemGenerico();
            try {
               var resultado = from p in db.producto
                                where p.id_prod == id_prod
                                                select new
                                                {
                                                    p.pre_prod
                                                };
                var listado = resultado.ToList();
                producto prod = new producto { pre_prod = listado[0].pre_prod };
                result.codigo = 0;
                result.mensaje = "exito";
                result.obj = prod;

                return Json(result, JsonRequestBehavior.AllowGet);
            } catch (Exception ex) {
                result.codigo = -1;
                result.mensaje = ex.Message;
                return null;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
