﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_dsw1.Data;
using Proyecto_dsw1.Models;
using Proyecto_dsw1.utils;

namespace Proyecto_dsw1.Controllers
{ 
    public class HomeController : Controller
    {
        private proyectoEntities db = new proyectoEntities();
        public ActionResult Pagina_principal()
        {
            dynamic mymodel = new ExpandoObject();
           // ViewBag.banner = bannerdao.Carrusel().ToList();
            //var productos = productoDao.ListaProducto();

            return View();
          
        }

        public ActionResult LoginAdmin() {
            return View();
        }
        [HttpPost]
        public ActionResult LoginAdmin(string vc_usuario,string vc_password)
        {
            login ok = db.login.Where(x => x.vc_usuario == vc_usuario && x.vc_password == vc_password).FirstOrDefault();
            if (ok != null)
            {

                Session["usuario"] = ok;
                if (ok.in_id == 1)
                {
                    return RedirectToAction("Mantenimiento_Home", "Home");

                }
                else {
                    return RedirectToAction("HomeEmpleado", "Home");
                }
               
                
            }
            else
            {
                ViewBag.mensaje = "Usuario y/o contraseña incorrecta";
            }
            return View();
        }

        public ActionResult Mantenimiento_Home()
        {
            ViewBag.paginaPadre = "Inicio";
            ViewBag.pagina = "Inicio";
            login usuarioSession = (login)Session["usuario"];


            if (usuarioSession != null)
            {
                Session["menu"] = db.menu_rol.ToList().Where(x => x.in_id_rol == usuarioSession.in_rol).OrderBy(x => x.menu.in_padre).ToList();
                return View();
            }
            else
            {
                if (verificarSession.sessionEmpleado("Home") == Constante.NO_LOGUEADO)
                    return RedirectToAction("LoginAdmin", "Home");
                else if (verificarSession.sessionEmpleado("Home") == Constante.LOGUEADO)
                    return View();
                else
                {
                    return RedirectToAction("LoginAdmin");
                }
            }
        }
        public ActionResult HomeEmpleado()
        {

            ViewBag.paginaPadre = "Home";
            ViewBag.pagina = "Inicio";
            login usuarioSession = (login)Session["usuario"];


            if (usuarioSession != null)
            {
                Session["menu"] = db.menu_rol.ToList().Where(x => x.in_id_rol == usuarioSession.in_rol).OrderBy(x => x.menu.in_padre).ToList();
                return View();
            }
            else
            {
                if (verificarSession.sessionEmpleado("Home") == Constante.NO_LOGUEADO)
                    return RedirectToAction("LoginAdmin", "Home");
                else if (verificarSession.sessionEmpleado("Home") == Constante.LOGUEADO)
                    return View();
                else
                {

                    return RedirectToAction("LoginAdmin");
                }
            }
            
        }
        [HttpGet]
        public ActionResult CerrarSesion()
        {
            Session.Remove("usuario");
            return RedirectToAction("LoginAdmin");
        }


        public JsonResult VentasMensual()
        {
            var ventas = db.SP_VENTAS_POR_SEMANA();

            var ventaSemanal = from p in ventas
                               select new { p.TOTAL };

            var ventaMensual = db.sp_ventas_por_Mes();


            var ventadiaria = db.sp_ventas_diarias();

            return Json(new { ventaSemanal = ventaSemanal.ToArray(), ventaMensual = ventaMensual, ventadiaria = ventadiaria }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetData2()
        {
            List<notificacion> messages = new List<notificacion>();
            Repository r = new Repository();
            messages = r.GetAllMessages();
            return Json(messages, JsonRequestBehavior.AllowGet);


        }
        public JsonResult getDashboard()
        {

            SP_DASHOBOARD_DATOS_Result datos = db.SP_DASHOBOARD_DATOS().FirstOrDefault();
            return Json(datos, JsonRequestBehavior.AllowGet);


        }
    }
}