﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_dsw1.Models;using Proyecto_dsw1.Data;

namespace Proyecto_dsw1.Controllers
{
    public class CarritoController : Controller
    {
        proyectoEntities db = new proyectoEntities();
        private int getIndex(int id=0)
        {
            List<Carrito> compra =(List<Carrito>)Session["carrito"];
            for(int i = 0; i < compra.Count; i++)
            {
                if (compra[i].producto.id_prod == id)
                    return i;
            }
            return -1;
        }
        public ActionResult AgregarCarrito()
        {
            return View();
        }
        //[HttpPost]
        //public ActionResult AgregarCarrito(int id_prod=0,int cantidad=0)
        //{
        //    if (Session["carrito"] == null)
        //    {
        //        List<Carrito> compras = new List<Carrito>();
        //        compras.Add(new Carrito(productodao.Buscar(id_prod),cantidad));
        //        Session["carrito"] = compras;
        //    }
        //    else
        //    {
        //        List<Carrito> compras = (List<Carrito>)Session["carrito"];
        //        int existente = getIndex(id_prod);
        //        if (existente == -1)
        //        {
        //            compras.Add(new Carrito(productodao.Buscar(id_prod), cantidad));
        //        }
        //        else
        //        {
        //            compras[existente].Cantidad+=cantidad;
                  

        //        }
        //        Session["carrito"] = compras;
        //    }
        //    return View();
        //}
        public ActionResult Delete(int id_prod = 0)
        {
            List<Carrito> compras = (List<Carrito>)Session["carrito"];
            compras.RemoveAt(getIndex(id_prod));
            return View("AgregarCarrito");
        }
        public ActionResult Details(int id = 0)
        {
            return View(db.producto.Find(id));
        }
    }
}