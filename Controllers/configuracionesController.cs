﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_dsw1.Data;

namespace Proyecto_dsw1.Controllers
{
    public class configuracionesController : Controller
    {
        private proyectoEntities db = new proyectoEntities();
        public ActionResult Edit()
        {
            ViewBag.paginaPadre = "Configuraciones";
            ViewBag.pagina = "Editar";
            ViewBag.accion = "";
            configuraciones configuraciones = db.configuraciones.ToList()[0];
            if (configuraciones == null)
            {
                return HttpNotFound();
            }
            return View(configuraciones);
        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "in_id,vc_nombre,vc_email,vc_celular,vc_telefono,vc_estado,vc_ruta_imagen,vc_ruta_corta,bt_imagen,dt_fec_reg,vc_user_reg,vc_user_mod,dt_fec_mod")] configuraciones configuraciones, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                guardarImagen(file);
                if (file != null)
                {
                    configuraciones.vc_ruta_imagen = Server.MapPath("~/imagenes").ToString() + "/" + file.FileName;
                    configuraciones.vc_ruta_corta = "../imagenes/" + file.FileName;
                    configuraciones.bt_imagen = ConversionImagen(configuraciones.vc_ruta_imagen);
                }
                db.Entry(configuraciones).State = EntityState.Modified;

                db.SaveChanges();
                ViewBag.mensaje = "success";

            }
            return View(configuraciones);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public static byte[] ConversionImagen(string nombrearchivo)
        {
            //Declaramos fs para poder abrir la imagen.
            FileStream fs = new FileStream(nombrearchivo, FileMode.Open);

            // Declaramos un lector binario para pasar la imagen
            // a bytes
            BinaryReader br = new BinaryReader(fs);
            byte[] imagen = new byte[(int)fs.Length];
            br.Read(imagen, 0, (int)fs.Length);
            br.Close();
            fs.Close();
            return imagen;
        }
        void guardarImagen(HttpPostedFileBase file)
        {

            if (file != null && file.ContentLength > 0)
            {
                var filename = Path.GetFileName(file.FileName);
                var ruta = Path.Combine(Server.MapPath("~/imagenes"), filename);
                file.SaveAs(ruta);
            }
        }
        public ActionResult EditarPagina()
        {
            ViewBag.paginaPadre = "Incio";
            ViewBag.pagina = "inicio";
            ViewBag.accion = "";
            configuraciones configuraciones = db.configuraciones.ToList()[0];
            if (configuraciones == null)
            {
                return HttpNotFound();
            }

            return View();
        }


    }
}
