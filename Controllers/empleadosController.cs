﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_dsw1.Data;
using Proyecto_dsw1.Entity;
using Proyecto_dsw1.utils;

namespace Proyecto_dsw1.Controllers
{
    public class empleadosController : Controller
    {
        private proyectoEntities db = new proyectoEntities();

        public ActionResult Index()
        {
            ViewBag.paginaPadre = "Empleados";
            ViewBag.pagina = "Listado";
            if (verificarSession.sessionEmpleado("Empleados") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if (verificarSession.sessionEmpleado("Empleados") == Constante.NO_AUTORIZADO)
                return RedirectToAction("LoginAdmin", "Home");
            else
                return View();
        }

        public JsonResult GetData()
        {
                db.Configuration.LazyLoadingEnabled = false;
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.Url.Query);
                string sEcho = nvc["sEcho"].ToString();
                string sSearch = nvc["sSearch"].ToString();
                int iDisplayStart = Convert.ToInt32(nvc["iDisplayStart"]);
                int iDisplayLength = Convert.ToInt32(nvc["iDisplayLength"]);

                int iSortCol = Convert.ToInt32(nvc["iSortCol_0"]);
                string sortOrder = nvc["sSortDir_0"].ToString();

                //get total value count
                var Count = db.empleado.Count();

                var Students = new List<empleado>();

                if (sSearch != "" && sSearch != null)
                {

                    Students = db.empleado.Where(a => a.VC_NOMBRE.ToLower().Contains(sSearch.ToLower())
                                      || a.VC_APELLIDO.ToLower().Contains(sSearch.ToLower())
                                      || a.VC_CELULAR.ToLower().Contains(sSearch.ToLower())
                                      ).Where(x => x.VC_ESTADO == "ACT")
                                      .ToList();

                    Count = Students.Count();
                    Students = SortFunction(iSortCol, sortOrder, Students).Skip(iDisplayStart).Take(iDisplayLength).ToList();
                }
                else
                {
                    Students = db.empleado.Where(x => x.VC_ESTADO == "ACT")
                                       .ToList();

                    Students = SortFunction(iSortCol, sortOrder, Students).Skip(iDisplayStart).Take(iDisplayLength).ToList();
                }

                var StudentsPaged = new SysDataTablePager<empleado>(Students, Count, Count, sEcho);

                return Json(StudentsPaged, JsonRequestBehavior.AllowGet);
            


        }
        private List<empleado> SortFunction(int iSortCol, string sortOrder, List<empleado> list)
        {

            //Sorting for String columns
            if (iSortCol == 1 || iSortCol == 0 || iSortCol == 2)
            {
                Func<empleado, string> orderingFunction = (c => iSortCol == 0 ? c.VC_NOMBRE : iSortCol == 1 ? c.VC_APELLIDO : iSortCol == 2 ? c.VC_EMAIL : c.VC_NOMBRE); // compare the sorting column

                if (sortOrder == "desc")
                {
                    list = list.OrderByDescending(orderingFunction).Where(x => x.VC_ESTADO == "ACT").ToList();
                }
                else
                {
                    list = list.OrderBy(orderingFunction).Where(x => x.VC_ESTADO == "ACT").ToList();

                }
            }


            return list;
        }

        public ActionResult Create()
        {
            if (verificarSession.sessionEmpleado("Empleados") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if (verificarSession.sessionEmpleado("Empleados") == Constante.NO_AUTORIZADO)
                return RedirectToAction("LoginAdmin", "Home");
            else
            {
                ViewBag.paginaPadre = "Empleados";
                ViewBag.pagina = "Nuevo";
                ViewBag.IN_SUCURSAL = new SelectList(db.sucursal, "in_id", "vc_nombre");
                ViewBag.in_id_rol = new SelectList(db.rol, "in_id_rol", "vc_descripcion");
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(string vc_nombre, string vc_apellido, string vc_celular, string vc_email,int? in_sucursal, string vc_usuario, string vc_password, string vc_user_reg,int in_id_rol)
        {
            if (verificarSession.sessionEmpleado("Empleados") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if (verificarSession.sessionEmpleado("Empleados") == Constante.NO_AUTORIZADO)
                return RedirectToAction("LoginAdmin", "Home");
            else
            {
                ViewBag.paginaPadre = "Empleados";
                ViewBag.pagina = "Nuevo";
                if (ModelState.IsValid)
                {

                    db.insertarEmpleado(vc_nombre, vc_apellido,vc_celular,vc_email,in_sucursal,vc_usuario,vc_password,vc_user_reg,in_id_rol);
                    return RedirectToAction("Index");
                }
                ViewBag.IN_SUCURSAL = new SelectList(db.sucursal, "in_id", "vc_nombre",in_sucursal);
                ViewBag.IN_ROL = new SelectList(db.rol, "in_id_rol", "vc_descripcion", in_id_rol);
                return View();
            }
        }

        public ActionResult Editar(int? id)
        {
            if (verificarSession.sessionEmpleado("Empleados") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if (verificarSession.sessionEmpleado("Empleados") == Constante.NO_AUTORIZADO)
                return RedirectToAction("LoginAdmin", "Home");
            else
            {
                ViewBag.paginaPadre = "Empleados";
                ViewBag.pagina = "Editar";

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                empleado empleado = db.empleado.Find(id);
                if (empleado == null)
                {
                    return HttpNotFound();
                }
                ViewBag.IN_SUCURSAL = new SelectList(db.sucursal, "in_id", "vc_nombre", empleado.IN_SUCURSAL);
                return View(empleado);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "in_id,VC_NOMBRE,VC_APELLIDO,VC_CELULAR,VC_ESTADO,VC_EMAIL,IN_SUCURSAL,VC_USER_MOD,DT_USER_MOD")] empleado empleado)
        {
            ViewBag.paginaPadre = "Empleados";
            ViewBag.pagina = "Editar";
            ViewBag.accion = "";
            if (ModelState.IsValid)
            {
                empleado.DT_USER_MOD = DateTime.Now;
                db.Entry(empleado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IN_SUCURSAL = new SelectList(db.sucursal, "in_id", "vc_nombre", empleado.IN_SUCURSAL);
            return View(empleado);
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            ViewBag.paginaPadre = "Empleados";
            ViewBag.pagina = "Listar";
            ViewBag.accion = "";
            empleado empleado = db.empleado.Find(id);
            empleado.VC_ESTADO = "ELIM";
            db.Entry(empleado).State = EntityState.Modified;
            db.SaveChanges();
            bool isAdmin = false;
            string output = isAdmin ? "Welcome to the Admin User" : "Welcome to the User";

            return Json(output, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            ViewBag.paginaPadre = "Mantenimientos";
            ViewBag.pagina = "empleados";
            ViewBag.accion = "Listado";
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
    }
