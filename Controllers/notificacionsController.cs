﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_dsw1.Data;
using Proyecto_dsw1.utils;

namespace Proyecto_dsw1.Controllers
{
    public class notificacionsController : Controller
    {
        private proyectoEntities db = new proyectoEntities();

        // GET: notificacions
        public ActionResult Index()
        {
            ViewBag.paginaPadre = "Notificaciones";
            ViewBag.pagina = "Listado";
            if (verificarSession.sessionEmpleado("Promociones") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if (verificarSession.sessionEmpleado("Promociones") == Constante.NO_AUTORIZADO)
                return RedirectToAction("LoginAdmin", "Home");
            else
            {
                return View(db.notificacion.ToList());
            }
        }

        // GET: notificacions/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.paginaPadre = "Notificaciones";
            ViewBag.pagina = "Detalle";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            notificacion notificacion = db.notificacion.Find(id);
            if (notificacion == null)
            {
                return HttpNotFound();
            }
            notificacion.vc_leido = "1";
            db.Entry(notificacion).State = EntityState.Modified;
            db.SaveChanges();
            return View(notificacion);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
