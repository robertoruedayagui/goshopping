﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_dsw1.Data;
using System.IO;

namespace Proyecto_dsw1.Controllers
{
    public class productosController : Controller
    {
        private proyectoEntities db = new proyectoEntities();

        // GET: productos
        public ActionResult Index()
        {
            ViewBag.paginaPadre = "Producto";
            ViewBag.pagina = "Listar";
            var producto = db.producto.Include(p => p.categoria).Include(p => p.genero);
            return View(producto.ToList());
        }

        // GET: productos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.producto.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // GET: productos/Create
        public ActionResult Create(int? id)
        {
            var imagenes = db.imagen_producto.Where(x => x.in_id_producto == id);
            ViewBag.listadoImagenes = from p in imagenes
                                      select new { p.vc_ruta_imagen};
            ViewBag.paginaPadre = "Producto";
            ViewBag.pagina = "Nuevo";
            ViewBag.id_cate = new SelectList(db.categoria, "id_cate", "desc_cate");
            ViewBag.id_prod = new SelectList(db.genero, "id_genero", "desc_genero");
            return View();
        }

        // POST: productos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_prod,vc_cod_prod,nom_prod,des_prod,pre_prod,marca,imagen,id_genero,id_cate,stock,activo")] producto producto)
        {
            ViewBag.paginaPadre = "Producto";
            ViewBag.pagina = "Nuevo";
            if (ModelState.IsValid)
            {
                db.producto.Add(producto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_cate = new SelectList(db.categoria, "id_cate", "desc_cate", producto.id_cate);
            ViewBag.id_prod = new SelectList(db.genero, "id_genero", "desc_genero", producto.id_prod);
            return View(producto);
        }

        // GET: productos/Edit/5
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.producto.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            ViewBag.paginaPadre = "Producto";
            ViewBag.pagina = "Listar";
            ViewBag.id_cate = new SelectList(db.categoria, "id_cate", "desc_cate", producto.id_cate);
            ViewBag.id_prod = new SelectList(db.genero, "id_genero", "desc_genero", producto.id_prod);
            return View(producto);
        }

        // POST: productos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_prod,vc_cod_prod,nom_prod,des_prod,pre_prod,marca,imagen,id_genero,id_cate,stock,activo")] producto producto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.paginaPadre = "Producto";
            ViewBag.pagina = "Listar";
            ViewBag.id_cate = new SelectList(db.categoria, "id_cate", "desc_cate", producto.id_cate);
            ViewBag.id_prod = new SelectList(db.genero, "id_genero", "desc_genero", producto.id_prod);
            return View(producto);
        }

        // GET: productos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.producto.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            producto producto = db.producto.Find(id);
            db.producto.Remove(producto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public static byte[] ConversionImagen(string nombrearchivo)
        {
            //Declaramos fs para poder abrir la imagen.
            FileStream fs = new FileStream(nombrearchivo, FileMode.Open);

            // Declaramos un lector binario para pasar la imagen
            // a bytes
            BinaryReader br = new BinaryReader(fs);
            byte[] imagen = new byte[(int)fs.Length];
            br.Read(imagen, 0, (int)fs.Length);
            br.Close();
            fs.Close();
            return imagen;
        }
        void guardarImagen(HttpPostedFileBase file)
        {

            if (file != null && file.ContentLength > 0)
            {
                var filename = Path.GetFileName(file.FileName);
                var ruta = Path.Combine(Server.MapPath("~/imagenes"), filename);
                file.SaveAs(ruta);
            }
        }
    }
}
