﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_dsw1.Entity;
using Proyecto_dsw1.Models;

using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.Helpers;
using Proyecto_dsw1.Data;
using System.Collections.Specialized;
using System.Data.Entity;
using Proyecto_dsw1.utils;
using System.Net;

namespace Proyecto_dsw1.Controllers
{
    public class ProductoController : Controller
    {
        proyectoEntities db = new proyectoEntities();
        public JsonResult GetData()
        {
            db.Configuration.LazyLoadingEnabled = false;
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.Url.Query);
            string sEcho = nvc["sEcho"].ToString();
            string sSearch = nvc["sSearch"].ToString();
            int iDisplayStart = Convert.ToInt32(nvc["iDisplayStart"]);
            int iDisplayLength = Convert.ToInt32(nvc["iDisplayLength"]);

            int iSortCol = Convert.ToInt32(nvc["iSortCol_0"]);
            string sortOrder = nvc["sSortDir_0"].ToString();

            //get total value count
            var Count = db.producto.Count();

            var Students = new List<producto>();

            if (sSearch != "" && sSearch != null)
            {

                Students = db.producto.Where(a => a.nom_prod.ToLower().Contains(sSearch.ToLower())
                                  || a.pre_prod.ToString().Contains(sSearch.ToLower())
                                  || a.des_prod.ToLower().Contains(sSearch.ToLower())
                                  ).Where(x => x.activo == true)
                                  .ToList();

                Count = Students.Count();
                Students = SortFunction(iSortCol, sortOrder, Students).Skip(iDisplayStart).Take(iDisplayLength).ToList();
            }
            else
            {
                Students = db.producto.Where(x => x.activo == true)
                                   .ToList();

                Students = SortFunction(iSortCol, sortOrder, Students).Skip(iDisplayStart).Take(iDisplayLength).ToList();
            }

            var StudentsPaged = new SysDataTablePager<producto>(Students, Count, Count, sEcho);

            return Json(StudentsPaged, JsonRequestBehavior.AllowGet);


        }
        private List<producto> SortFunction(int iSortCol, string sortOrder, List<producto> list)
        {

            //Sorting for String columns
            if (iSortCol == 1 || iSortCol == 0 || iSortCol == 2)
            {
                Func<producto, string> orderingFunction = (c => iSortCol == 0 ? c.nom_prod : iSortCol == 1 ? c.des_prod : iSortCol == 2 ? c.marca : c.nom_prod); // compare the sorting column

                if (sortOrder == "desc")
                {
                    list = list.OrderByDescending(orderingFunction).Where(x => x.activo == true).ToList();
                }
                else
                {
                    list = list.OrderBy(orderingFunction).Where(x => x.activo == true).ToList();

                }
            }


            return list;
        }

        public ActionResult Index(int p=0)
        {

            ViewBag.paginaPadre = "Producto";
            ViewBag.pagina = "Listar";
            ViewBag.accion = "";
            if (verificarSession.sessionEmpleado("Producto") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if (verificarSession.sessionEmpleado("Producto") == Constante.LOGUEADO)
                return View();
            else
                return RedirectToAction("LoginAdmin", "Home");
        }
        public ActionResult listaPorGenero(int genero= 0)
        {
            return View(db.producto.Where(x => x.genero.id_genero == genero));
        }
        [HttpPost]
        public ActionResult Buscar(String nombre)
        {
            return View(db.producto.Where(x => x.des_prod == nombre));
        }
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            ViewBag.paginaPadre = "Producto";
            ViewBag.pagina = "Listar";
            ViewBag.accion = "";
            if (file != null && file.ContentLength > 0)
            {
                var filename = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/App_Data/imagenes"), filename);
                file.SaveAs(path);
            }
            return View(db.producto.ToList());
        }
        //public ActionResult Create()
        //{
        //    ViewBag.paginaPadre = "Producto";
        //    ViewBag.pagina = "Nuevo";
        //    ViewBag.accion = "";
        //    ViewBag.categorias = new SelectList(daocategoria.ListaCategoria(), "id_cate", "desc_cate");
        //    ViewBag.generos = new SelectList(daogenero.listaGenero(), "id_genero", "desc_genero");
        //    return View(new Producto());
        //}
        //[HttpPost]
        //public ActionResult Create(Producto pro)
        //{
        //    ViewBag.paginaPadre = "Producto";
        //    ViewBag.pagina = "Nuevo";
        //    ViewBag.accion = "";
        //    try
        //    {

        //        if (ModelState.IsValid)
        //        {
        //            ViewBag.categorias = new SelectList(daocategoria.ListaCategoria(), "id_cate", "desc_cate");
        //            ViewBag.generos = new SelectList(daogenero.listaGenero(), "id_genero", "desc_genero");

        //            HttpPostedFileBase filebase = Request.Files[0];
        //            WebImage image = new WebImage(filebase.InputStream);
        //            pro.imagen = image.GetBytes();

        //            if (filebase != null && filebase.ContentLength > 0)
        //            {
        //                var filename = Path.GetFileName(filebase.FileName);
        //                var path = Path.Combine(Server.MapPath("~/App_Data/imagenes"), filename);
        //                filebase.SaveAs(path);
        //            }

        //            daoproducto.insertaProducto(pro);

        //            return RedirectToAction("index");
        //        }
        //        return RedirectToAction("index");
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.mensaje = ex.ToString();
        //        return View();
        //    }
        ////}
        //public ActionResult Edit(int? id_prod)
        //{
        //    ViewBag.paginaPadre = "Producto";
        //    ViewBag.pagina = "Editar";
        //    ViewBag.accion = "";
        //    ViewBag.categorias = new SelectList(daocategoria.ListaCategoria(), "id_cate", "desc_cate");
        //    ViewBag.generos = new SelectList(daogenero.listaGenero(), "id_genero", "desc_genero");
        //    if (id_prod == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    producto pro = db.producto.Find(id_prod);
        //    if (pro == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    if (verificarSession.sessionEmpleado("Producto") == Constante.NO_LOGUEADO)
        //        return RedirectToAction("LoginAdmin", "Home");
        //    else if (verificarSession.sessionEmpleado("Producto") == Constante.LOGUEADO)
        //        return View(pro);
        //    else
        //        return RedirectToAction("LoginAdmin", "Home");
        //}
        //[HttpPost]
        //public ActionResult Edit([Bind(Include = "id_prod,vc_cod_prod,nom_prod,des_prod,pre_prod,marca,id_genero")]producto pro)
        //{
        //    ViewBag.paginaPadre = "Producto";
        //    ViewBag.pagina = "Nuevo";
        //    ViewBag.accion = "";
        //    try
        //    {
        //        ViewBag.categorias = new SelectList(daocategoria.ListaCategoria(), "id_cate", "desc_cate", pro.id_cate);
        //        ViewBag.generos = new SelectList(daogenero.listaGenero(), "id_genero", "desc_genero", pro.id_cate);

        //        if (ModelState.IsValid)
        //        {
        //            db.Entry(pro).State = EntityState.Modified;
        //            db.SaveChanges();
        //            return RedirectToAction("Index");
        //        }
        //        return RedirectToAction("index");
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.mensaje = ex.ToString();
        //        return View();
        //    }
        //}
        [HttpPost]
        public JsonResult Delete(int id)
        {
            ViewBag.paginaPadre = "Mantenimientos";
            ViewBag.pagina = "empleados";
            producto prod = db.producto.Find(id);
            prod.activo = false;
            db.Entry(prod).State = EntityState.Modified;
            db.SaveChanges();
            string output = "success";

            return Json(output, JsonRequestBehavior.AllowGet);
        }


        public ActionResult getImage(int id_prod = 0)
        {

            producto prod = db.producto.Find(id_prod);
            if (prod.imagen != null)
            {
                byte[] byteimage = prod.imagen;

                MemoryStream ms = new MemoryStream(byteimage);
                Image image = Image.FromStream(ms);

                ms = new MemoryStream();
                image.Save(ms, ImageFormat.Jpeg);
                ms.Position = 0;

                return File(ms, "image/jpg");
            }
            else
            { 
                return null;
            }
        }
    }
}