﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_dsw1.utils;
using Proyecto_dsw1.Data;

namespace Proyecto_dsw1.Controllers
{
    public class VentasController : Controller
    {
        proyectoEntities db = new proyectoEntities();
        // GET: Ventas
        public ActionResult Index()
        {
            ViewBag.paginaPadre = "Pedidos";
            ViewBag.pagina = "Listado";
            if (verificarSession.sessionEmpleado("Producto") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if (verificarSession.sessionEmpleado("Producto") == Constante.NO_AUTORIZADO)
                return RedirectToAction("LoginAdmin", "Home");
            else
                return View();
        }
        public JsonResult listadoPedidos(string fechaini,string fechaFin,string numpedido,string estado) {
            DateTime fechaInicial = new DateTime(1999, 1, 1);
            DateTime fechaFinal = new DateTime(1999, 1, 1);
            if (fechaFin != "")
            {
                fechaInicial = Convert.ToDateTime(fechaini);

            }
            if (fechaFin != "") {
                fechaFinal = Convert.ToDateTime(fechaFin);
            }
            var pedidoXFecha = db.usp_lista_pedidos(fechaInicial, fechaFinal, numpedido, estado);

            return Json(pedidoXFecha.ToList(), JsonRequestBehavior.AllowGet);
        }
       
    }
}