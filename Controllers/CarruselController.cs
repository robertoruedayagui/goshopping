﻿ using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using Proyecto_dsw1.Data;
using Proyecto_dsw1.utils;

namespace Proyecto_dsw1.Controllers
{
    public class CarruselController : Controller
    {
        proyectoEntities db = new proyectoEntities();
        public ActionResult Index()
        {
            ViewBag.paginaPadre = "Banner";
            ViewBag.pagina = "Inicio";
            ViewBag.accion = "";
          //  ViewBag.banner = db.banner.ToList();

           
            if (verificarSession.sessionEmpleado("Banner") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if(verificarSession.sessionEmpleado("Banner") == Constante.LOGUEADO)
                return View(new banner());
            else
                return RedirectToAction("LoginAdmin", "Home");
        }

        [HttpPost]
        public ActionResult Create(banner ban)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    HttpPostedFileBase filebase = Request.Files[0];
                    WebImage image = new WebImage(filebase.InputStream);
                    ban.desc_banner = image.GetBytes();

                    if (filebase != null && filebase.ContentLength > 0)
                    {
                        var filename = Path.GetFileName(filebase.FileName);
                        var path = Path.Combine(Server.MapPath("~/App_Data/imagenes"), filename);
                        filebase.SaveAs(path);
                    }

                    db.insertarImagen(ban.desc_banner);

                    return RedirectToAction("index");
                }
                return RedirectToAction("index");
            }
            catch (Exception ex)
            {
                ViewBag.mensaje = ex.ToString();
                return View();
            }

        }
        public JsonResult GetData()
        {


            var imagenes = db.banner.ToList();

            var resultado = from p in imagenes
                            select new { p.id_banner };

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            string output = "";
            int codigo = 0;
            try
            {
                buscarImagen_Result ban = db.buscarImagen(id).FirstOrDefault();

                if (db.banner.ToList().Count <= 1)
                {
                    output = "No se pudo eliminar, minimo debe tener una imágen";
                    codigo = 0;
                }
                else
                {
                    db.eliminarImagen(id);
                    output = "Se eliminó correctamente";
                    codigo = 1;
                }
            }
            catch (Exception ex)
            {
                output = "No se pudo eliminar la imágen";
                codigo = 0;
            }
            return Json(new { codigo, output }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getImage(int id_prod = 0)
        {

            buscarImagen_Result ban = db.buscarImagen(id_prod).FirstOrDefault();
            byte[] byteimage = ban.desc_banner;

            MemoryStream ms = new MemoryStream(byteimage);
            Image image = Image.FromStream(ms);

            ms = new MemoryStream();
            image.Save(ms, ImageFormat.Jpeg);
            ms.Position = 0;

            return File(ms, "image/jpg");
        }
    }
}