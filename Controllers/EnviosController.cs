﻿using HtmlAgilityPack;
using Proyecto_dsw1.utils;
using RadPdf.Web.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Web_Colegio.utils;

namespace Proyecto_dsw1.Controllers
{
    public class EnviosController : Controller
    {
        // GET: Envios
        public ActionResult Index()
        {
            if (verificarSession.sessionEmpleado("Banner") == Constante.NO_LOGUEADO)
                return RedirectToAction("LoginAdmin", "Home");
            else if (verificarSession.sessionEmpleado("Banner") == Constante.NO_AUTORIZADO)
                return RedirectToAction("LoginAdmin", "Home");
            else {  
                ViewBag.paginaPadre = "Envio";
                ViewBag.pagina = "Correo";

                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(string para, string asunto, string body, HttpPostedFileBase file)
        {
            string url = ConfigurationManager.AppSettings["ipPublica"].ToString();
            ViewBag.paginaPadre = "Envio";
            ViewBag.pagina = "Correo";
            var doc = new HtmlDocument();
            doc.LoadHtml(body);
            foreach (var node in doc.DocumentNode.SelectNodes("//img"))
            {
                var src = node.Attributes[@"src"].Value;
                if (src.StartsWith("/"))
                    node.SetAttributeValue("src", url + src);
            }

            var newHtml = doc.DocumentNode.WriteTo();
            EnvioAsincrono(para, asunto, body).Wait();
            return View();
        }
        public ActionResult Cuerpo()
        {
            ViewBag.paginaPadre = "Envio";
            ViewBag.pagina = "Correo";
            return View();
        }
        public async Task EnvioAsincrono(string correo, string asunto, string body)
        {
            EnvioMail mail = new EnvioMail();
            mail.EnvioCorreo("Roberto", asunto, correo, body);
        }
        public void getImage(byte[] binario)
        {
            byte[] byteimage = binario;

            MemoryStream ms = new MemoryStream(byteimage);
            Image image = Image.FromStream(ms);

            ms = new MemoryStream();
            image.Save(ms, ImageFormat.Jpeg);
            ms.Position = 0;

            // return File(ms, "image/jpg");


        }

        public static byte[] ConversionImagen(string nombrearchivo)
        {
            //Declaramos fs para poder abrir la imagen.
            FileStream fs = new FileStream(nombrearchivo, FileMode.Open);

            // Declaramos un lector binario para pasar la imagen
            // a bytes
            BinaryReader br = new BinaryReader(fs);
            byte[] imagen = new byte[(int)fs.Length];
            br.Read(imagen, 0, (int)fs.Length);
            br.Close();
            fs.Close();
            return imagen;
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LoadFile(HttpPostedFileBase file)
        {
            var vReturnImagePath = string.Empty;
            if (file.ContentLength > 0)
            {
                var vFileName = Path.GetFileNameWithoutExtension(file.FileName);
                var vExtension = Path.GetExtension(file.FileName);

                string sImageName = vFileName + DateTime.Now.ToString("YYYYMMDDHHMMSS");

                var vImageSavePath = Server.MapPath("~/imagenes/") + sImageName + vExtension;
                //sImageName = sImageName + vExtension;  
                vReturnImagePath = "/imagenes/" + sImageName + vExtension;
                ViewBag.Msg = vImageSavePath;
                var path = vImageSavePath;

                // Saving Image in Original Mode  
                file.SaveAs(path);
                var vImageLength = new FileInfo(path).Length;
                //here to add Image Path to You Database ,  
                TempData["message"] = string.Format("Image was Added Successfully");
            }
            return Json(Convert.ToString(vReturnImagePath), JsonRequestBehavior.AllowGet);
        }

    }
}