﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_dsw1.utils
{
    public static class Constante
    {
        public const int NO_LOGUEADO = 0;
        public const int LOGUEADO = 1;
        public const int NO_AUTORIZADO = 2;

        public const int SOLES = 1;
        public const int PORCENTAJE = 2;

        public const int ENERO = 1;
        public const int FEBRERO = 2;
        public const int MARZO = 3;
        public const int ABRIL = 4;
        public const int MAYO = 5;
        public const int JUNIO = 6;
        public const int JULIO = 7;
        public const int AGOSTO = 8;
        public const int SEPTIEMBRE = 9;
        public const int OCTUBRE = 10;
        public const int NOVIEMBRE = 11;
        public const int DICIEMBRE = 12;

        public const string MES = "1;Ene|2;Feb|3;Mar|4;Abr|5;May|6;Jun|7;Jul|8;Ago|9;Sep|10;Oct|11;Nov:Dic";

    }
}