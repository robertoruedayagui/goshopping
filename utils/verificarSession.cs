﻿using Proyecto_dsw1.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_dsw1.utils
{
    public static class verificarSession
    {
        public static int sessionEmpleado(string nombrePagina) {
            int result = 0;
            login usuarioSession = (login)HttpContext.Current.Session["usuario"];
            List<menu_rol> menu = new List<menu_rol>();

            if (usuarioSession == null)
            {
                result = Constante.NO_LOGUEADO;

            }
            else
            {
                if (HttpContext.Current.Session["menu"] != null)
                {
                    menu = (List<menu_rol>)HttpContext.Current.Session["menu"];
                    int count = menu.Where(x => x.menu.vc_nombre == nombrePagina && x.int_id_accion !=0 ).ToList().Count;
                    if (menu != null && count > 0)
                    {
                        result = Constante.LOGUEADO;
                    }
                    else
                    {
                        result = Constante.NO_AUTORIZADO;

                    }
                }
                else
                {
                    result = Constante.NO_LOGUEADO;
                }
            }
            return result;
        }
    }
}