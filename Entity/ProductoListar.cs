﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_dsw1.Entity
{
    public class ProductoListar
    {
        public string id_prod { get; set; }
        public string nom_prod { get; set; }
        public string desc_prod { get; set; }
        public double pre_prod { get; set; }
        public string marca { get; set; }
        public string genero { get; set; }
        public string categoria { get; set; }
        public int activo { get; set; }
        public byte imagen { get; set; }
    }
}