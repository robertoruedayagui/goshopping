﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.ComponentModel.DataAnnotations;

namespace Proyecto_dsw1.Entity
{
    public class Registrarse
    {

        [Display(Name ="Nombre(s) : ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ingrese Nombre")]
        public string nombre { get; set; }

        [Display(Name = "Apellido(s) : ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ingrese Apellido")]
        public string apellido { get; set; }

        [Display(Name = "Dni : ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ingrese DNI")]
        [RegularExpression(@"\d{8}",ErrorMessage = "DNI tiene 8 digitos")]
        public string dni { get; set; }

        [Display(Name = "E-mail : ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ingrese Correo")]
        [EmailAddress(ErrorMessage ="Ingrese correctamente el Email")]
        public string correo { get; set; }

        [Display(Name = "Contraseña : ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ingrese Contraseña")]
        public string clave { get; set; }

        [Display(Name = "Teléfono/Celular : ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ingrese Teléfono/Celular")]
        public string telefono { get; set; }

        [Display(Name = "Dirección : ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ingrese Dirección")]
        public string direccion { get; set; }

        [Display(Name = "Distrito : ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ingrese Distrito")]
        public string distrito { get; set; }

    }
}