//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Proyecto_dsw1.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class detalle_venta
    {
        public int id_venta { get; set; }
        public int id_produ { get; set; }
        public Nullable<int> id_concepto { get; set; }
        public Nullable<decimal> precio { get; set; }
        public Nullable<decimal> dc_desc { get; set; }
        public Nullable<decimal> dc_prec_final { get; set; }
        public Nullable<int> cantidad { get; set; }
        public string vc_estado { get; set; }
        public string vc_cupon { get; set; }
        public string vc_user_reg { get; set; }
        public Nullable<System.DateTime> dt_fec_reg { get; set; }
        public string vc_user_mod { get; set; }
        public Nullable<System.DateTime> dt_fec_mod { get; set; }
    
        public virtual producto producto { get; set; }
        public virtual ventas ventas { get; set; }
    }
}
