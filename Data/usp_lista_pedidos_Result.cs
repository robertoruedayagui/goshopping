//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Proyecto_dsw1.Data
{
    using System;
    
    public partial class usp_lista_pedidos_Result
    {
        public int id_venta { get; set; }
        public string cliente { get; set; }
        public string vc_num_pedido { get; set; }
        public string fecha_reg { get; set; }
        public string hora_reg { get; set; }
        public Nullable<decimal> DT_SUBTOTAL { get; set; }
        public Nullable<decimal> DT_DESCUENTO { get; set; }
        public Nullable<decimal> total { get; set; }
        public string ESTADO { get; set; }
    }
}
