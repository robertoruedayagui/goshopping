﻿using Proyecto_dsw1.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Proyecto_dsw1.Models
{
    public class Repository
    {
        private proyectoEntities db = new proyectoEntities();
        SqlConnection con = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        public List<notificacion> GetAllMessages()
        {
            var messages = new List<notificacion>();
         
            using (var cmd = new SqlCommand(@"SELECT [in_id_notificacion],[vc_descripcion],[vc_tipo],[vc_leido],[dt_fec_reg],[in_id_referencia] FROM [dbo].[notificacion] where [vc_leido] = '0' ", con))
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                var dependency = new SqlDependency(cmd);

                dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);


                //if (dependency.HasChanges)
                //{
                DataSet ds = new DataSet();
                da.Fill(ds);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    messages.Add(item: new notificacion
                    {
                        in_id_notificacion = int.Parse(ds.Tables[0].Rows[i][0].ToString()),
                        vc_descripcion = ds.Tables[0].Rows[i][1].ToString(),
                        vc_tipo = ds.Tables[0].Rows[i][2].ToString(),
                        vc_leido = ds.Tables[0].Rows[i][3].ToString(),
                        dt_fec_reg = DateTime.Parse( ds.Tables[0].Rows[i][4].ToString()),
                        in_id_referencia = int.Parse(ds.Tables[0].Rows[i][5].ToString()),
                    });
                }
            }
            return messages;
        }
        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {

            if (e.Type == SqlNotificationType.Change)
            {
                MyHub.SendMessages();
            }


        }
    }
}