﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Proyecto_dsw1.Data;
using Proyecto_dsw1.Entity;


namespace Proyecto_dsw1.Models
{
    public class Carrito
    {
        private producto _producto;
            public producto producto
        {
            get { return _producto; }
            set { _producto = value; }
        }
        private int _cantidad;
        public int Cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }
        public Carrito()
        {

        }
        public Carrito(producto producto,int cantidad)
        {
            this._producto = producto;
            this._cantidad = cantidad;
        }
    }
}